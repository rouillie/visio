# Open-source visio-conferencing systems

We propose scripts to easily install some efficient and open-source visio-conferencing solutions

- [BigBlueButton](https://gitlab.inria.fr/rouillie/bbb)
- [Galene](galene/README.md)

## Usages

There are mainly 3 kinds of usages :
- Public visio-conferences : large number of webcams simultaneously open, large number of attendees;
- Webinars : moderate number of webcams simultaneously open, very large number of attendees;
- Private visio-conferences (small number of attendees)

## For Public visio-conferences : Bigbluebutton

### Requirements

- powefull servers (standard : 8/16 core 16/32 GB of ram - can work on 4 cores and 8 GB of memory) : it processes the information (video and sound) from the clients to optimize the flows it will send back to all the clients
- reasonable power for the client side in terms of processor and network : the client processes each video flow but they have been reworked by the server to minimize the effort.

### Usage 

- (+) a full graphical administration program
- (+) a lot of functions
- (-) initialization of the connexion is quite slow (many tests, in particular the echo test)

### Installation of the server 

- (-) Quite heavy (30 minutes) and might be tricky especially behind a NAT.
- (-) Needs a dedicated server 
- (-) Exclusive use of the ports http and https by default

## For Webinars : Galene

### Requirements

- small servers are sufficient : it does not rework the video nor the sound 
- the client effort is proportional to the number of video shared (screen of webcam) in terms of processor but also in terms of network: the client processes each video/audio flow left untouch by the server.

### Usage 

- (-) Configuration/commands : rudimentary but impressively efficient.
    - Text configuration files for rooms/user accounts
    - Text commands for live operations
- (+) initialization of the connexion is very fast
- (-) No difference between shaing a camera and sharing a screenn (size of the windows)
- (+) Pip (Picture in picture) allows to detach any flow in a separate window and to increase its size

### Installation of the server 

- (+) very easy, few minutes.
- (+) does not require a dedicated server
- (+) can easily be installed behind a domestic router
- (+) allows to customize the ports used

## Installations scripts

- [BigBlueButton](https://gitlab.inria.fr/rouillie/bbb)
- [Galene](galene/README.md)
