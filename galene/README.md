[Galene](https://galene.org/) is a small and highly efficent system that might be used for webinars with a large number of attendees of for personal visio-conferences. It is not designed for conferences with a large number of webcams simultaneously openned.

## Requirements

- a linux on a machine (virtual or not) running **Ubuntu 20.04** or ** Debian Buster **.
- a domain name `<MY_DOMAIN_NAME>` with a subdomain prefixed by `<VISIO_MACHINE_NAME>` such that Galene can be access at 
`<VISIO_MACHINE_NAME>.<MY_DOMAIN_NAME>:8443`

>**Note** : Galene works also on ARM Architectures as well as with ohter linux distributions. You should be able to adapt easily the scripts, just take care of installing a version og go greater or equal than 1.13.

## Behind a NAT

**WARNING** : *as the default installation will install a turn server on the same machine as galene, you might possibly face problems if  hairpinning  is not set in your NAT. Some options of coturn server are supposed to solve some of them but it is not clear that it will always work. Nevertheless, the script can easily be adapted in order to use an externat turn server. For the time being it has been successively tested on a public cloud (Scaleway) and behind a 10 years old freobox (v6)*.

If the machine where Galene will be installed is behind a NAT, then you need to open the following ports and to forward them to those of your target machine 

- 49152-65535 UDP 
- 3478 (UDP/TCP)
- 8443 (UDP/TCP) 

>**Note** : you will also need to forward temporarily the port 80 for generating SSL certificates during the installation process

## Installation

Connect as root on your machine for simplicity and then type :

	git clone https://gitlab.inria.fr/rouillie/visio
	./visio/galene/install_galene.sh <VISIO_MACHINE_NAME> <MY_DOMAIN_NAME> <CERTBOT_MAIL>

Enjoy !

## Update

For updating your galene, just update the git directory and re-run the installaton script withut parameters. Connect as root on your machine and type :

	git pull https://gitlab.inria.fr/rouillie/visio
	./visio/galene/install_galene.sh

