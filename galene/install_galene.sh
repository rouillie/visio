#!/bin/bash

full_command=$(realpath $0)
galene_install_dir=$(dirname $0)

if [ ! -f $galene_install_dir/galene.env ]; then
    echo "***********************************************************************";
    if [ "$#" -lt 3 ]; then
	echo ""
	echo "First use : $0 <VISIO_MACHINE_NAME> <MY_DOMAIN_NAME> <CERTBOT_MAIL>"
	echo ""
    exit;
    else
	echo ""
	echo "Mofify parameters in $galene_install_dir/galene.env"
	echo ""
    fi
    
cat > $galene_install_dir/galene.env <<HERE                                                                                                                 
VISIO_MACHINE_NAME=$1
MY_DOMAIN_NAME=$2
CERTBOT_MAIL=$3
#where galene git will be cloned
GALENE_COMPIL_DIR=/root
#where galene will be installed
GALENE_HOME_DIR=/home/galene
#the unix user and group
GALENE_USER=galene
#credientials for connecting to the COTURN
TURN_USER_NAME=$(date +%s%N | cut -b10-19)
TURN_USER_PASSWD=$(date +%s%N | cut -b10-19)
#credientials for the test room
GALENE_OPERATOR=$(date +%s%N | cut -b10-19)
GALENE_OPERATOR_PASSWORD=$(date +%s%N | cut -b10-19)

HERE

else  
   echo "***********************************************************************";
   echo ""
   echo "Re-use parameters from $galene_install_dir/galene.env"
   echo ""
fi

source ${galene_install_dir}/galene.env

TURN_MACHINE_NAME=${VISIO_MACHINE_NAME}

echo "***********************************************************************";
echo Install Galene on ${VISIO_MACHINE_NAME}.${MY_DOMAIN_NAME}

# check DNS

echo "***********************************************************************";
echo "Check DNS entries"
echo ""

internal_ip=$(hostname -I | cut -f1 -d' ')
external_ip=$(curl http://ifconfig.me)
domain_ip=$(host "${VISIO_MACHINE_NAME}.${MY_DOMAIN_NAME}" | awk '{print $4;exit;}')

if [ "x$domain_ip" == "xfound:" ];then
    domain_ip="Not Found";
fi

if [ "$domain_ip" != "$external_ip" ]; then
    echo ""
    echo "DNS IP ($domain_ip) and EXTERNAL IP ($external_ip) differ ";
    echo ""
    echo "Verify that the DNS entry ${VISIO_MACHINE_NAME}.${MY_DOMAIN_NAME} is set to IP $external_ip";
    exit;
else
    echo "DNS IP ($domain_ip) and EXTERNAL IP ($external_ip) fit";
fi

if [ ! -f ${GALENE_HOME_DIR}/galene ]; then

    echo "***********************************************************************";
    echo "First Installation"
    echo "Update/Upgrade your system"

    apt update -y
    apt upgrade -y

    echo "***********************************************************************";
    echo "Install GitLab compiler"
    apt install -y git

    echo "***********************************************************************";
    echo "Install Go compiler"
    apt install -y golang-go

    echo "***********************************************************************";
    echo "Install Certbot"
    apt -y install certbot

    echo "***********************************************************************";
    echo "Install COTURN"

    apt -y install coturn
    systemctl stop coturn

fi

# Check/generate SSL CERTIFICATES

if [ ! -d /etc/letsencrypt/live/${VISIO_MACHINE_NAME}.${MY_DOMAIN_NAME} ]; then
   sudo certbot certonly -n --standalone -d ${VISIO_MACHINE_NAME}.${MY_DOMAIN_NAME} -m ${CERTBOT_MAIL} --agree-tos
fi

## Configure Coturn

echo "TURNSERVER_ENABLED=1" > /etc/default/coturn

cat > /etc/turnserver.conf <<HERE
listening-port=3478
fingerprint
lt-cred-mech
user=${TURN_USER_NAME}:${TURN_USER_PASSWD}
server-name=${TURN_MACHINE_NAME}.${MY_DOMAIN_NAME}
realm=${MY_DOMAIN_NAME}
HERE

## Configure Coturn

systemctl start coturn

# Galene

echo "***********************************************************************";
echo "Install Galene"

## Compile Galene

mkdir -p ${GALENE_COMPIL_DIR}
cd ${GALENE_COMPIL_DIR}

rm -rf  ${GALENE_COMPIL_DIR}/galene
git clone https://github.com/jech/galene
echo  ${GALENE_COMPIL_DIR}
ls ${GALENE_COMPIL_DIR}
cd ${GALENE_COMPIL_DIR}/galene
ls

res=$(CGO_ENABLED=0 go build -ldflags='-s -w')

exit 0

## Install Galene


mkdir -p ${GALENE_HOME_DIR}

useradd -U -m ${GALENE_USER} -d ${GALENE_HOME_DIR}

mkdir -p ${GALENE_HOME_DIR}/{data,groups}

rsync -a ${GALENE_COMPIL_DIR}/galene/galene ${GALENE_COMPIL_DIR}/galene/static ${GALENE_HOME_DIR}

cp /etc/letsencrypt/live/${VISIO_MACHINE_NAME}.${MY_DOMAIN_NAME}/fullchain.pem ${GALENE_HOME_DIR}/data/cert.pem
cp /etc/letsencrypt/live/${VISIO_MACHINE_NAME}.${MY_DOMAIN_NAME}/privkey.pem ${GALENE_HOME_DIR}/data/key.pem
chown galene:galene ${GALENE_HOME_DIR}/data/*.pem
chmod go-rw ${GALENE_HOME_DIR}/data/key.pem

### Configure Galene's turn


cat > ${GALENE_HOME_DIR}/data/ice-servers.json <<HERE
[
    {
        "Urls": [
            "turn:${TURN_MACHINE_NAME}.${MY_DOMAIN_NAME}:3478",
            "turn:${TURN_MACHINE_NAME}.${MY_DOMAIN_NAME}:3478?transport=tcp"
        ],
        "username": "${TURN_USER_NAME}",
        "credential": "${TURN_USER_PASSWD}"
    }
]
HERE

### Create a test room 

cat > ${GALENE_HOME_DIR}/groups/test.json <<HERE
{
    "op": [{"username": "${GALENE_OPERATOR}", "password": "${GALENE_OPERATOR_PASSWORD}"}],
    "presenter": [{}]
}
HERE

### Set the service Galene

cat >  /etc/systemd/system/galene.service <<HERE

[Unit]
Description=Galene
After=network.target

[Service]
Type=simple
WorkingDirectory=${GALENE_HOME_DIR}
User=galene
Group=galene
ExecStart=${GALENE_HOME_DIR}/galene
LimitNOFILE=65536

[Install]
WantedBy=multi-user.target

HERE

systemctl enable galene.service
systemctl start galene.service
systemctl status galene.service


echo "***********************************************************************";
echo ""
echo " Try your galene server !"
echo ""
echo " URL :  https://${TURN_MACHINE_NAME}.${MY_DOMAIN_NAME}:8443/group/test"
echo " Username: ${GALENE_OPERATOR}"
echo " Password: ${GALENE_OPERATOR_PASSWORD}"
echo "***********************************************************************";
